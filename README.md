# Python fft

Fast Fourier Transform of the famous "La"

filtering.py calculate FFT of the signal "la.wav".
Normally we have to found 440Hz but here we get 443Hz which is higher than the usuall.

Don't forget :
```math
f=\frac{1}{L}\sqrt{\frac{T}{\mu}}
```
with L the wire's lenght, T the wire's tension and $\mu$ is the linear mass.

In this case we need decrease the wire's tension.

How to run in terminal : python2 filtering.py

Python's Module needed : matplotlib, numpy, soundfile, sounddevice, time


![filtering.py ouput](signals.png)
