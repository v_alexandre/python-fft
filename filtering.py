from matplotlib import pyplot as plt
import numpy as np
import soundfile as sf
import sounddevice as sd
import time

signal, samplerate = sf.read('la.wav')
samplerate=float(samplerate)
tend = np.size(signal)/samplerate
t = np.linspace(0,tend,np.size(signal))

## FFT signal
dt = tend/np.size(signal)
n = signal.size
fhat = np.fft.fft(signal,n)
PSD = fhat*np.conjugate(fhat)/n
f = np.fft.fftfreq(n, d=dt)

## filter noise
indices = PSD>100
PSDclean = PSD*indices

## use the PSD to filter out noise
fhat = indices*fhat
ffilt = np.fft.ifft(fhat)

## plots
fig, axs = plt.subplots(3, 1)
axs[0].plot(t,signal)
axs[0].set_xlabel("time s")
axs[0].set_ylabel("signal")
axs[0].grid(True)

axs[1].plot(f[0:len(f)/2],PSD[0:len(PSD)/2],label="PSD")
axs[1].plot(f[0:len(f)/2],PSDclean[0:len(PSDclean)/2],label="PSD clean")
axs[1].set_ylabel("PSD")
axs[1].set_xlabel("Frequency Hz")
axs[1].grid(True)
axs[1].legend()

print("Fmax=",abs(f[np.where(PSDclean==np.max(PSDclean))][0]))

axs[2].plot(t,ffilt)
axs[2].set_xlabel("time s")
axs[2].set_ylabel("signal clean")
axs[2].grid(True)

fig.tight_layout()
plt.show()

## play sound
sd.play(signal, samplerate)
status = sd.wait()  # Wait until file is done playing
time.sleep(1) # code wait 1s
sd.play(np.real(ffilt), samplerate)
status = sd.wait()  # Wait until file is done playing

